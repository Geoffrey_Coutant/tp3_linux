# TP 3 : We do a little scripting

## I. Script carte d'identité

vous pouvez trouver l'intérieur de [idcard.sh](ressource/idcard.sh)
```
[user1@localhost srv]$ mkdir idcard
[user1@localhost idcard]$ touch idcard.sh
[user1@localhost idcard]$ ./idcard.sh

Machine name : localhost 
Os Rocky Linux release 9.0 (Blue Onyx) and kernel version is #1 SMP Tue Sep 20 16:15:14 UTC 2022
IP : 192.168.64.9 fdde:389b:285a:ae3c:788:11db:532:b49f 
RAM : 675988 KB memory available on 973692 KB total memory
Disk : 606032 KB space left
Top 5 processes by RAM usage : 
-/usr/bin/python3
-/usr/lib/polkit-1/polkitd
-/usr/sbin/NetworkManager
-/usr/lib/systemd/systemd
-/usr/lib/systemd/systemd
Listening ports :
  - udp 323 
  - tcp 22 
Here is your random cat : ./cat.png

[user1@localhost idcard]$ ls -al

total 36
drwxr-xr-x. 2 user1 user1    38 Dec 13 08:50 .
drwxr-xr-x. 3 root  root     35 Dec 12 11:52 ..
-rw-r--r--. 1 user1 user1 30490 Dec 13 09:08 cat.png
-rwxr-xr-x. 1 user1 root    998 Dec 13  2022 idcard.sh
```

## II. Script youtube-dl

vous pouvez trouver l'intérieur de [yt.sh](ressource/yt.sh)

```
[user1@localhost /]$ cd /var/log/
[user1@localhost log]$ sudo mkdir yt
[user1@localhost yt]$ sudo touch download.log
[user1@localhost yt]$ ./yt.sh https://www.youtube.com/watch?v=BXanQaSVQzg

[youtube] BXanQaSVQzg: Downloading webpage
[download] Destination: /srv/yt/downloads/la vanne de l'hôtel/la vanne de l'hôtel.mp4
[download] 100% of 1.08MiB in 00:16
Video https://www.youtube.com/watch?v=BXanQaSVQzg was downloaded.
File path : /srv/yt/downloads/la vanne de l'hôtel/la vanne de l'hôtel.mp4

[user1@localhost yt]$ cat /var/log/yt/download.log

[12/13/22 10:09:18] Video https://www.youtube.com/watch?v=BXanQaSVQzg was downloaded. File path : /srv/yt/downloads/la vanne de l'hôtel/la vanne de l'hôtel.mp4
```

## III. MAKE IT A SERVICE

vous pouvez trouver l'intérieur de [yt-v2.sh](ressource/yt-v2.sh) 

```
[user1@localhost yt]$ sudo to
[user1@localhost yt]$ sudo chown user1:user1 /srv/yt/yt-v2.shuch yt-v2.sh
[user1@localhost yt]$ systemctl status yt

● yt.service - petit service illegal pour download des films xD
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-12-13 11:50:59 CET; 1min 59s ago
   Main PID: 2226 (yt-v2.sh)
      Tasks: 3 (limit: 5876)
     Memory: 43.6M
        CPU: 3.261s
     CGroup: /system.slice/yt.service
             ├─2226 /bin/bash /srv/yt/yt-v2.sh
             ├─2227 bash /srv/yt/yt.sh "https://www.youtube.com/watch?v=fVQtrP4FWPs"
             └─2231 python /usr/local/bin/youtube-dl "https://www.youtube.com/watch?v=fVQtrP4FWPs" "--output=/srv/yt/downloads/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles.mp4"

Dec 13 11:50:59 localhost.localdomain systemd[1]: Started petit service illegal pour download des films xD.
Dec 13 11:51:02 localhost.localdomain yt-v2.sh[2231]: [youtube] fVQtrP4FWPs: Downloading webpage
Dec 13 11:51:02 localhost.localdomain yt-v2.sh[2231]: [download] Resuming download at byte 11321067
Dec 13 11:51:03 localhost.localdomain yt-v2.sh[2231]: [download] Destination: /srv/yt/downloads/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles.mp4

```

```
[user1@localhost yt]$ journalctl -xe -u yt
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A stop job for unit yt.service has begun execution.
░░ 
░░ The job identifier is 1589.
Dec 13 11:50:59 localhost.localdomain yt-v2.sh[2187]: [7.9K blob data]
Dec 13 11:50:59 localhost.localdomain systemd[1]: yt.service: Deactivated successfully.
░░ Subject: Unit succeeded
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ The unit yt.service has successfully entered the 'dead' state.
Dec 13 11:50:59 localhost.localdomain systemd[1]: Stopped petit service illegal pour download des films xD.
░░ Subject: A stop job for unit yt.service has finished
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A stop job for unit yt.service has finished.
░░ 
░░ The job identifier is 1589 and the job result is done.
Dec 13 11:50:59 localhost.localdomain systemd[1]: yt.service: Consumed 1min 36.282s CPU time.
░░ Subject: Resources consumed by unit runtime
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ The unit yt.service completed and consumed the indicated resources.
Dec 13 11:50:59 localhost.localdomain systemd[1]: Started petit service illegal pour download des films xD.
░░ Subject: A start job for unit yt.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░ 
░░ A start job for unit yt.service has finished successfully.
░░ 
░░ The job identifier is 1589.
Dec 13 11:51:02 localhost.localdomain yt-v2.sh[2231]: [youtube] fVQtrP4FWPs: Downloading webpage
Dec 13 11:51:02 localhost.localdomain yt-v2.sh[2231]: [download] Resuming download at byte 11321067
Dec 13 11:51:03 localhost.localdomain yt-v2.sh[2231]: [download] Destination: /srv/yt/downloads/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles.mp4
Dec 13 11:54:41 localhost.localdomain yt-v2.sh[2231]: [11.7K blob data]
Dec 13 11:54:43 localhost.localdomain yt-v2.sh[2227]: Video https://www.youtube.com/watch?v=fVQtrP4FWPs was downloaded.
Dec 13 11:54:43 localhost.localdomain yt-v2.sh[2227]: File path : /srv/yt/downloads/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles/Essayer De Ne Pas Rire 😂 Meilleurs Chats et Chiens Drôles 😺🐶 Partie 1 | Animaux Drôles.mp4
Dec 13 11:54:43 localhost.localdomain yt-v2.sh[2227]: /srv/yt/yt.sh: line 32: /var/log/yt/download.log: Permission denied
```
